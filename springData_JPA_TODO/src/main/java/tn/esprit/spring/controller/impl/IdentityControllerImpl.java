/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.spring.controller.impl;

import org.springframework.stereotype.Controller;

import tn.esprit.spring.controller.interfaces.IidentityController;
import tn.esprit.spring.entity.Client;

/**
 * 
 * Cette classe représente une implémentation de l'interface <code>IidentityController</code> .
 * Cette classe implémente les méthodes qui permettent de gérer l'identité d'un client.
 * 
 * @author Walid YAICH
 *
 */

@Controller
public class IdentityControllerImpl implements IidentityController {
	
	//TODO Injecter le service (il faut utiliser l'interface)
	
	/**
	 * Récupérer le nom et prénom d'un client en ayant son Id
	 * @param clientId  l'identifiant du client
	 * @return prénom et nom du client
	 */
	@Override
	public String getFullNameByClientId(Long clientId){
		return null; //TODO
	}


	/**
	 * Ajouter un client dans la base
	 * @param client  l'objet client a rajouter dans la base
	 * @return Client l'entité client sychronisée avec la base
	 */
	@Override
	public Client addClient(Client client) {
		return null; //TODO
	}


	/**
	 * Supprimer un client
	 * @param le client a supprimer
	 */
	@Override
	public void deleteClient(Client client) {
		//TODO
	}


	
	/**
	 * Calculer le nombre de clients dans la base
	 */
	@Override
	public Long countClients() {
		return null; //TODO
	}

	/**
	 * Récupérer le nombre de client ayant le nom {@code lastName}
	 * @param lastName
	 * @return le nombre de client ayant le nom {@code lastName}
	 */
	@Override
	public Long countClientsByName(String lastName) {
		return null; //TODO
	}
	
	/**
	 * Récupérer l'id d'un client en ayant son nom et prénom
	 * return Long l'id du client
	 */
	@Override
	public Long findIdByFirstNameAndLastName(String firstName, String lastName) {
		return null; //TODO
	}

}
