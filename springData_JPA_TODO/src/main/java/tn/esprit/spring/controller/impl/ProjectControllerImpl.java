/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.spring.controller.impl;

import org.springframework.stereotype.Component;
import tn.esprit.spring.controller.interfaces.IprojectController;
import tn.esprit.spring.entity.Project;


/**
 * 
 * Cette classe sert a implémenter les méthodes qui permettent de gérer un projet.
 * Cette classe représente une implémentation de l'interface <code>IidentityController</code>
 * 
 * @author Walid YAICH
 *
 */
@Component
public class ProjectControllerImpl implements IprojectController  {

	//TODO Injecter le service (il faut utiliser l'interface)
	
	/**
	 * Récupérer les titres des projets en ayant l'id du client
	 * @param clientId l'identifiant du client
	 * @return les titres des projets 
	 */
	@Override
	public String getProjectsTitlesByClientId(Long clientId){
		return null;//TODO
	}

	/**
	 * Ajouter un projet dans la base
	 * @param project l'object a insérer dans la base
	 */
	@Override
	public void addProject(Project project) {
		//TODO
	}

	/**
	 * Récupérer le titre d'un projet en ayant son ID
	 * @param projectid
	 * @return String le titre du projet
	 */
	@Override
	public String getProjectTitle(Long projectid) {
		return null;//TODO
	} 
	
	
	/**
	 * Supprimer tous les projets
	 */
	@Override
	public void deleteAllProjects() {
		//TODO
	}
	
}
